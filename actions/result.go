package actions

import (
	"encoding/json"

	"gitlab.com/f5-pwe/common/v2/duration"
	"gitlab.com/f5-pwe/common/v2/variables"
)

type ActionResult struct {
	Name         string             `json:"step_name" yaml:"step_name"`
	Result       ActionStatus       `json:"result" yaml:"result"`
	ReturnCode   int                `json:"rc" yaml:"rc"`
	Message      string             `json:"message" yaml:"message"`
	NotifyStatus string             `json:"notify_status,omitempty" yaml:"notify_status,omitempty"`
	Duration     duration.Duration  `json:"duration" yaml:"duration"`
	Context      *variables.Context `json:"context" yaml:"context"`
}

type tmpAR ActionResult

func (a *ActionResult) UnmarshalJSON(b []byte) (err error) {
	var newAR tmpAR
	if err = json.Unmarshal(b, &newAR); err == nil {
		*a = ActionResult(newAR)
	}
	return
}
