/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package actions

import (
	"encoding/json"
	"fmt"
	"testing"
)

func Test_SimpleActionsUnmarshalJSON(t *testing.T) {
	var actionsJson = []byte(`{
        "action1": {
            "image": "test",
            "version": "passed"
        }
    }`)
	var actions Actions

	if err := json.Unmarshal(actionsJson, &actions); err == nil {
		if action, err1 := actions.Action("action1"); err1 == nil {
			if action.Name != "action1" {
				t.Error("Action name does not match")
			}
			if action.Image != "test" {
				t.Error("Action Image does not match")
			}
			if action.Tag != "passed" {
				t.Error("Action version does not match")
			}
		} else {
			fmt.Println(err1)
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_ActionsResurnsErrorForMissingAction(t *testing.T) {
	var actionsJson = []byte(`{
        "action1": {
            "image": "test",
            "version": "passed"
        }
    }`)
	var actions Actions

	if err := json.Unmarshal(actionsJson, &actions); err == nil {
		_, err1 := actions.Action("action2")
		if err1 == nil {
			t.Error("Dit not get error for missing action")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_ActionsUnmarshalJSON(t *testing.T) {
	var actionsJson = []byte(`{
        "action1": {
            "image": "test",
            "version": "passed",
            "command": "bla",
            "args": [
                "--one"
            ],
            "env": {
                "key": "value"
            }
        },
        "action2": {
            "image": "test2",
            "version": "passed2",
            "command": "bla2",
            "args": [
                "--one",
                "--two"
            ],
            "env": {
                "key": "value2"
            }
        }
    }`)
	var actions Actions

	if err := json.Unmarshal(actionsJson, &actions); err == nil {
		if action, err1 := actions.Action("action1"); err1 == nil {
			if action.Name != "action1" {
				t.Error("Action name does not match")
			}
			if action.Image != "test" {
				t.Error("Action Image does not match")
			}
			if action.Tag != "passed" {
				t.Error("Action version does not match")
			}
			if action.Command != "bla" {
				t.Error("Action command does not match")
			}
			if action.Args[0] != "--one" {
				t.Error("Action args do not match")
			}
			if action.Env["key"] != "value" {
				t.Error("Action env does not match")
			}
		} else {
			fmt.Println(err1)
			t.FailNow()
		}
		if action2, ok := actions["action2"]; ok {
			if action2.Name != "action2" {
				t.Error("Action name does not match")
			}
			if action2.Image != "test2" {
				t.Error("Action Image does not match")
			}
			if action2.Tag != "passed2" {
				t.Error("Action version does not match")
			}
			if action2.Command != "bla2" {
				t.Error("Action command does not match")
			}
			if action2.Args[0] != "--one" && action2.Args[1] != "--two" {
				t.Error("Action args do not match")
			}
			if action2.Env["key"] != "value2" {
				t.Error("Action env does not match")
			}
		} else {
			fmt.Println("Action did not exist")
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_SimpleActionResultUnmarshalJSON(t *testing.T) {
	var actionResultJson = []byte(`{
        "rc": 0,
        "result": "success",
        "message": "",
        "context": {
            "key": "value"
        }
    }`)
	var ar ActionResult

	if err := json.Unmarshal(actionResultJson, &ar); err == nil {
		if ar.ReturnCode != 0 {
			t.Error("ActionResult rc does not match")
		}
		if ar.Result != ACTION_SUCCESS {
			t.Error("ActionResult result does not match")
		}
		if ACTION_SUCCESS.String() != "success" {
			t.Error("ActionStatus does not match")
		}
		if (*ar.Context)["key"] != "value" {
			t.Error("ActionResult context does not match")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}
