package actions

type Usage struct {
	Memory string `json:"memory" yaml:"memory"`
	CPU    string `json:"cpu" yaml:"cpu"`
}

type PodResources struct {
	Requests *Usage `json:"requests" yaml:"requests"`
	Limits   *Usage `json:"limits" yaml:"limits"`
}
