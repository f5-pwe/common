package actions

import (
	"gitlab.com/f5-pwe/common/v2/variables"
)

type Requirements struct {
	Env          variables.Args `json:"env,omitempty" yaml:"env,omitempty"`
	ContextPaths variables.Args `json:"context_paths,omitempty" yaml:"context_paths,omitempty"`
}

type Output struct {
	Context *variables.Context `json:"context,omitempty" yaml:"context,omitempty"`
}
