ifndef GOPATH
$(warning You need to set up a GOPATH. Run "go help gopath".)
endif

go:=$(strip $(shell which go 2> /dev/null))
ifneq ($(go),)
	packages:=$(shell $(go) list ./... | grep -v /vendor/)
endif

.PHONY: test format vendor

default: test

deps:
	go get -u -t ./...
	go mod tidy -compat=1.22
	go mod vendor

format:
	$(go) fmt $(packages)

test: format
	$(go) vet $(packages)
	echo "mode: set" > coverage-all.out
	$(foreach pkg,$(packages),\
		go test -v -race -coverprofile=coverage.out $(pkg) || false;\
		tail -n +2 coverage.out >> coverage-all.out;)
	go tool cover -func=coverage-all.out

