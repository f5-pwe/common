/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package workflow

import (
	"encoding/json"
	"fmt"
	"testing"
)

func Test_SimpleWorkflows(t *testing.T) {
	var workflowsJson = []byte(`{
        "workflowA": {
            "start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        }
    }`)
	var wf Workflows

	if err := json.Unmarshal(workflowsJson, &wf); err == nil {
		if _, err1 := wf.Workflow("workflowA"); err1 != nil {
			fmt.Println(err1)
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_SimpleWorkflowExists(t *testing.T) {
	var workflowsJson = []byte(`{
        "workflowA": {
            "start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        }
    }`)
	var wf Workflows

	if err := json.Unmarshal(workflowsJson, &wf); err == nil {
		if !wf.Exists("workflowA") {
			t.Error("Workflow does not exist")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_MultipleWorkflows(t *testing.T) {
	var workflowsJson = []byte(`{
        "workflowA": {
            "start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        },
        "workflowB": {
            "start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        }
    }`)
	var wf Workflows

	if err := json.Unmarshal(workflowsJson, &wf); err == nil {
		if _, err1 := wf.Workflow("workflowA"); err1 != nil {
			fmt.Println(err1)
			t.FailNow()
		}
		if _, err1 := wf.Workflow("workflowB"); err1 != nil {
			fmt.Println(err1)
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_EachWorkflowRequiresStart(t *testing.T) {
	var workflowsJson = []byte(`{
        "workflowA": {
            "not_start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        }
    }`)
	var wf Workflows

	if err := json.Unmarshal(workflowsJson, &wf); err == nil {
		t.Error("error expected while loading invalid workflow")
	}
}

func Test_IndividualWorkflowDeserializes(t *testing.T) {
	var workflowsJson = []byte(`{
        "workflowA": {
            "start": {
                "action": "test",
                "success": "passed"
            },
            "passed": {
                "action": "valid"
            }
        }
    }`)
	var wfz Workflows

	if err := json.Unmarshal(workflowsJson, &wfz); err == nil {
		if wf, err1 := wfz.Workflow("workflowA"); err1 == nil {
			if wfs, err1 := wf.Step("start"); err1 == nil {
				if wfs.Name != "start" {
					t.Error("Step name does not match")
				}
				if wfs.ActionName != "test" {
					t.Error("Step ActionName does not match")
				}
				if wfs.SuccessStep != "passed" {
					t.Error("Step SuccessStep does not match")
				}
			} else {
				fmt.Println(err1)
				t.FailNow()
			}
		} else {
			fmt.Println(err)
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_SimpleWorkflowUnmarshalJSON(t *testing.T) {
	var workflowJson = []byte(`{
        "start": {
            "action": "test",
            "success": "passed"
        },
        "passed": {
            "action": "valid"
        }
    }`)
	var wf Workflow

	if err := json.Unmarshal(workflowJson, &wf); err == nil {
		if wfs, err1 := wf.Step("start"); err1 == nil {
			if wfs.Name != "start" {
				t.Error("Step name does not match")
			}
			if wfs.ActionName != "test" {
				t.Error("Step ActionName does not match")
			}
			if wfs.SuccessStep != "passed" {
				t.Error("Step SuccessStep does not match")
			}
		} else {
			fmt.Println(err1)
			t.FailNow()
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_WorkflowUnmarshalJSONValidatesWorkflowSteps(t *testing.T) {
	var workflowJson = []byte(`{
        "start": {
            "action": "test",
            "success": "passed"
        }
    }`)
	var wf Workflow

	err := json.Unmarshal(workflowJson, &wf)
	if err == nil {
		t.Error("Unmarshaled invalid workflow")
	}
}

func Test_WorkflowErrorIfNoStartStep(t *testing.T) {
	var workflowJson = []byte(`{
        "step1": {
            "action": "test",
            "success": "passed"
        },
        "passed": {
            "action": "valid"
        }
    }`)
	var wf Workflow

	err := json.Unmarshal(workflowJson, &wf)
	if err == nil {
		t.Error("Unmarshaled invalid workflow")
	}
}
