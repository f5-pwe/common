/*
Copyright 2018 F5 Networks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package pwe

import (
	"encoding/json"
	"fmt"
	"strconv"
	"testing"
	"time"
)

func Test_SimpleJobUnmarshalJSON(t *testing.T) {
	var jobJson = []byte(`{
        "correlation_id": "9k1q7i17da80",
        "name": "test job def",
        "workflows": {
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {
            "key": "value"
        }
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Name != "test job def" {
			t.Error("Job Name does not match")
		}
		if job.ID.String() != "9k1q7i17da80" {
			t.Error("Job ID does not match")
		}
		nano, _ := job.ID.UnixNano()
		if nano != int64(1257894000000000000) {
			t.Error("Job ID does not match nano")
		}
		created, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", "2009-11-10 15:00:00 -0800 PST")
		if job.CreatedDate.UTC() != created.UTC() {
			t.Error("Job CreatedTime does not match")
		}
		if job.Status != JOB_QUEUED {
			t.Error("Job Status does not match")
		}
		if action, err := job.Actions.Action("action1"); err == nil {
			if action.Name != "action1" {
				t.Error("Action name does not match")
			}
			if action.Image != "test" {
				t.Error("Action Image does not match")
			}
			if action.Tag != "passed" {
				t.Error("Action version does not match")
			}
		} else {
			fmt.Println(err)
			t.FailNow()
		}
		wf, err := job.Workflows.Workflow("run")
		if err != nil {
			fmt.Println(err)
			t.FailNow()
		}
		if wfs, err := wf.Step("start"); err == nil {
			if wfs.Name != "start" {
				t.Error("WorkflowStep name does not match")
			}
			if wfs.ActionName != "test" {
				t.Error("WorkflowStep ActionName does not match")
			}
			if wfs.SuccessStep != "passed" {
				t.Error("WorkflowStep SuccessStep does not match")
			}
		} else {
			fmt.Println(err)
			t.FailNow()
		}

	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobDatesUnmarshalJSON(t *testing.T) {
	var jobJson = []byte(`{
        "correlation_id": "9k1q7i17da80",
        "name": "test job def",
        "start_date": "2009-11-10T15:00:00-08:00",
        "end_date": "2009-11-12T15:00:00-08:00",
        "workflows": {
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		started, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", "2009-11-10 15:00:00 -0800 PST")
		started = started.UTC()
		if job.StartDate.UTC() != started {
			t.Error("Job StartDate does not match")
		}
		ended, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", "2009-11-12 15:00:00 -0800 PST")
		ended = ended.UTC()
		if job.EndDate.UTC() != ended {
			t.Error("Job EndDate does not match")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobWorkflowIsRequired(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	err := json.Unmarshal(jobJson, &job)
	if err == nil {
		t.Error("Unmarshaled without workflow")
	}
}

func Test_JobCurrentWorkflowDefaults(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "workflows": {
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err != nil {
		t.Error(err.Error())
	}
	if job.CurrentWorkflow != "run" {
		t.Error("CurrentWorkflow did not default")
	}
}

func Test_JobCurrentWorkflowIsUsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "current_workflow": "workflowA",
        "workflows":{
            "workflowA": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            },
            "workflowB": {
                "start": {
                    "action": "test",
                    "success": "failed"
                },
                "failed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err != nil {
		t.Error(err.Error())
	}

	if wfs, err := job.Workflow().Step("start"); err == nil {
		if wfs.Name != "start" {
			t.Error("WorkflowStep name does not match")
		}
		if wfs.ActionName != "test" {
			t.Error("WorkflowStep ActionName does not match")
		}
		if wfs.SuccessStep != "passed" {
			t.Error("WorkflowStep SuccessStep does not match")
		}
	} else {
		t.Error(err.Error())
	}
}

func Test_JobActionsIsRequired(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "context": {}
    }`)
	var job Job

	err := json.Unmarshal(jobJson, &job)
	if err == nil {
		t.Error("Unmarshaled without actions")
	}
}

func Test_JobIDIsCreated(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		id := string(job.ID)
		if id == "0" || id == "" {
			fmt.Println("ID was not generated")
			t.FailNow()
		}
		nano, _ := job.ID.UnixNano()
		if id != strconv.FormatInt(nano, 36) {
			t.Error("ID was not generated")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobStatusQueuedIsParsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "status": "queued",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Status != JOB_QUEUED {
			t.Error("Status was not parsed")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobStatusRunningIsParsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "status": "running",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Status != JOB_RUNNING {
			t.Error("Status was not parsed")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobStatusPendingResourcesIsParsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "status": "pending resources",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Status != JOB_PENDING_RESOURCES {
			t.Error("Status was not parsed")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobStatusFailureIsParsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "status": "failure",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Status != JOB_FAILURE {
			t.Error("Status was not parsed")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_JobStatusSuccessIsParsed(t *testing.T) {
	var jobJson = []byte(`{
        "name": "test job def",
        "status": "success",
        "workflows":{
            "run": {
                "start": {
                    "action": "test",
                    "success": "passed"
                },
                "passed": {
                    "action": "valid"
                }
            }
        },
        "actions": {
            "action1": {
                "image": "test",
                "version": "passed"
            }
        },
        "context": {}
    }`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Status != JOB_SUCCESS {
			t.Error("Status was not parsed")
		}
		if JOB_SUCCESS.String() != "success" {
			t.Error("Status was not converted")
		}
	} else {
		fmt.Println(err)
		t.FailNow()
	}
}

func Test_ComplexJobUnmarshalJSON(t *testing.T) {
	var jobJson = []byte(`{
    "name": "dev-smoke-vmware",
    "description": "Execute a minimal smoke run in dev VMware environment",
    "workflows": {
        "run": {
            "start": {
                "action": "harness-destroy",
                "fail": "notify",
                "abort": "notify",
                "success": "setup"
            },
            "setup": {
                "action": "harness-provision",
                "fail": "notify",
                "abort": "notify",
                "success": "configure"
            },
            "configure": {
                "action": "harness-configure",
                "fail": "notify",
                "abort": "notify",
                "success": "run",
				"timeout": "notify",
				"duration": "3m30s"
            },
            "run": {
                "action": "sleuth-run",
                "fail": "notify",
                "abort": "notify",
                "success": "tear-down"
            },
            "notify": {
                "action": "failure-notify",
                "fail": "",
                "abort": "",
                "success": ""
            },
            "tear-down": {
                "action": "harness-destroy",
                "fail": "notify",
                "abort": "notify",
                "success": ""
            }
        },
        "remediate": {
            "start": {
                "action": "harness-destroy",
                "fail": "notify",
                "abort": "notify",
                "success": ""
            },
            "notify": {
                "action": "failure-notify",
                "fail": "",
                "abort": "",
                "success": ""
            }
        }
    },
    "actions": {
        "harness-provision": {
            "image": "docker.fake.url:5000/pwe/provision-onearmed-vmware",
            "version": "dev",
            "env": {
                "SSH_BASTION_HOST": "dev-vebvt-vmware-01-bastion.fake.url"
            }
        },
        "harness-configure": {
            "image": "docker.fake.url:5000/pwe/configure-onearmed",
            "version": "dev",
            "env": {
                "SSH_BASTION_HOST": "dev-vebvt-vmware-01-bastion.fake.url"
            }
        },
        "sleuth-run": {
            "image": "docker.fake.url:5000/pwe/sleuth-runner",
            "version": "dev",
            "args": [],
            "env": {
                "SSH_BASTION_HOST": "dev-vebvt-vmware-01-bastion.fake.url"
            }
        },
        "failure-notify": {
            "image": "docker.fake.url:5000/pwe/failure-notify",
            "version": "dev",
            "env": {
                "SSH_BASTION_HOST": "dev-vebvt-vmware-01-bastion.fake.url"
            }
        },
        "harness-destroy": {
            "image": "docker.fake.url:5000/pwe/teardown-onearmed-vmware",
            "version": "dev",
            "env": {
                "SSH_BASTION_HOST": "dev-vebvt-vmware-01-bastion.fake.url"
            }
        }
    },
    "context": {
        "chroot_url": "http://artifactory.fake.url/CURRENT.tgz",
        "tc_branch": "bigip11.6.1",
        "sleuth": {
            "args": [
                "--tdf=support/tdf/SMOKE.tdf"
            ]
        },
        "vmware_bastion": "dev-vebvt-vmware-01-bastion.fake.url",
        "vmware_hostname": "dev-vebvt-vmware-01.fake.url",
        "vmware_password": "password",
        "vmware_user": "user"
    }
}`)
	var job Job

	if err := json.Unmarshal(jobJson, &job); err == nil {
		if job.Name != "dev-smoke-vmware" {
			t.Error("Job Name does not match")
		}
		if job.Status != JOB_QUEUED {
			t.Error("Job Status does not match")
		}
		if action, err := job.Actions.Action("harness-provision"); err == nil {
			if action.Name != "harness-provision" {
				t.Error("Action name does not match")
			}
		} else {
			fmt.Println(err)
			t.FailNow()
		}
		wf := job.Workflow()
		if wfs, err := wf.Step("start"); err == nil {
			if wfs.Name != "start" {
				t.Error("WorkflowStep name does not match")
			}
		} else {
			fmt.Println(err)
			t.FailNow()
		}

		if wfs, err := wf.Step("configure"); err == nil {
			if wfs.Timeout.Duration != 3*time.Minute+30*time.Second {
				t.Error("WorkflowStep Timeout does not match")
			}
		}

	} else {
		fmt.Println(err)
		t.FailNow()
	}
}
